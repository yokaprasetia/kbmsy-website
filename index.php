<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="icon" href="img/welcome-image1.png">
    <title>KBMSY</title>
</head>

<body>
    <!-------------------------------NAVBAR SECTION------------------------------->
    <div id="navbar" class="navbar">
        <h1><a href="index.php">KBMSY</a></h1>
        <ul>
            <li><a href="#mahasiswa">Mahasiswa</a></li>
            <li><a href="#developer">Developer</a></li>
            <li><a href="#signIn">sign In</a></li>
        </ul>
    </div>

    <div class="scroll">
        <a href="#navbar"><img class="scroll" src="img/all-scroller.png"></a>
        <p>Back to Top</p>
    </div>
    <!-------------------------------ENDSECTION------------------------------->
    <div class="container">

        <div class="welcome" id="welcome">
            <div class="text">
                <h1>Selamat Datang di Keluarga Besar Mahasiswa STIS Yogyakarta</h1>
            </div>
            <img src="img/welcome-image1.png">
        </div>

        <div class="stis" id="stis">
            <div class="kiri">
                <h2>Apa Itu KBMSY?</h2>
                <p>KBMSY adalah singkatan dari Keluarga Besar Mahasiswa STIS Yogyakarta. Jika Anda tahu, STIS adalah salah satu Perguruan Tinggi Kedinasan(PTK) yang terletak di Jakarta Timur, tepatnya di Jl. Otto Iskandar Dinata Nomor 64C.</p>
            </div>
            <div class="kanan">
                <img class="kiri" src="img/stis-image1.png">
                <img class="tengah" src="img/stis-image2.png">
                <img class="kanan" src="img/stis-image3.png">
            </div>
        </div>

        <div class="mahasiswa" id="mahasiswa">
            <h2>Visit Our Member</h2>
            <img class="" src="img/index-developer.png">
            <a href="">lihat daftar Mahasiswa</a>
        </div>

        <div class="slider" id="slider">
            <div class="slides">
                <input type="radio" name="radio-btn" id="radio1">
                <input type="radio" name="radio-btn" id="radio2">
                <input type="radio" name="radio-btn" id="radio3">
                <input type="radio" name="radio-btn" id="radio4">

                <img class="slide first" src="img/gallery-image1.png">
                <img class="slide" src="img/gallery-image2.png">
                <img class="slide" src="img/gallery-image3.png">
                <img class="slide" src="img/gallery-image4.png">

            </div>

            <div class="navigation-manual">
                <label for="radio1" class="manual-btn"></label>
                <label for="radio2" class="manual-btn"></label>
                <label for="radio3" class="manual-btn"></label>
                <label for="radio4" class="manual-btn"></label>
            </div>
        </div>

        <div class="bubble">
            <div class="kotak kiri">
                <p>ADD BUBBLE</p>
            </div>
            <div class="kotak kanan">
                <p>REMOVE BUBBLE</p>
            </div>
        </div>
        <div class="developer" id="developer">
            <div class="shape none"></div>
            <h2>Hidden Rights</h2>
            <img src="img/index-developer.png">
            <a href="">login as developer here</a>
        </div>

    </div>


    <!-------------------------------FOOTER SECTION------------------------------->
    <div class="footer">
        <div class="atas">
            <h2>TENTANG KAMI</h2>
            <ul class="satu">
                <li>
                    <a href="#navbar"><img src="img/all-footer1.png"></a>
                    <p>Facebook</p>
                </li>
                <li>
                    <a href="#navbar"><img src="img/all-footer2.png"></a>
                    <p>Youtube</p>
                </li>
                <li>
                    <a href="#navbar"><img src="img/all-footer3.png"></a>
                    <p>Instagram</p>
                </li>
            </ul>
            <ul class="dua">
                <li><a href="https://www.privacypolicyonline.com/live.php?token=QnxTyjK8BwG98PMkI8kyYLNrnJDmDPNM" target="_blank">Syarat & Ketentuan</a></li>
                <li><a href="https://www.privacypolicyonline.com/live.php?token=8VWPLfEIYCFrTOl4unrgu2AwHnfMSOxX" target="_blank">Kebijakan Privasi</a></li>
            </ul>
        </div>
        <div class="bawah">
            <h2>Keluarga Besar Mahasiswa <a href="https://stis.ac.id" target="_blank">STIS</a> Yogyakarta</h2>
            <p>YOGYAKARTA&copy;2021</p>
            <p>KBMSY</p>
        </div>
    </div>
    <!-------------------------------ENDSECTION------------------------------->

    <script src="javascript/script.js"></script>
</body>

</html>