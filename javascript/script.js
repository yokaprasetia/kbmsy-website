const body = document.body;
const scroll = document.querySelector('div.scroll');
const stisKiri = document.querySelector('.stis .kanan img.kiri');
const stisTengah = document.querySelector('.stis .kanan img.tengah');
const stisKanan = document.querySelector('.stis .kanan img.kanan');
const navbar = document.getElementById('navbar');
const welcomeH1 = document.querySelector('.welcome .text h1');
const welcomeImg = document.querySelector('.welcome img');
const shape = document.querySelector('.developer .shape');
const developer = document.querySelector('.developer');
const developerH2 = document.querySelector('.developer h2');
const developerLink = document.querySelector('.developer a');
const kiri = document.querySelector('.bubble .kiri');
const kanan = document.querySelector('.bubble .kanan');


window.addEventListener('scroll', function() {
    let value = window.scrollY;
    console.log(value);

    if ( value > 2964 ) {
        body.style.backgroundPositionY = -1 * (-500 + value) + 'px';
    } else if ( value < 2964 ) {
        body.style.backgroundPositionY = '0px';
    }

    if ( value >= 60 ) {
        scroll.style.display = 'block';
    } else if ( value < 60 ){
        scroll.style.display = 'none';
    }

    stisKiri.style.transform = 'translate(0px,'+ (900 - value*1.5) +'px)';
    stisTengah.style.transform = 'translate(-60px,'+ (390 + value/-1.8) +'px)';
    stisKanan.style.transform = 'translate(-120px,'+ (700 - value/1.16) +'px)';


    
});

navbar.classList.add('landing');

scroll.style.display = 'none';

welcomeH1.classList.add('landing');
welcomeImg.classList.add('landing');

window.addEventListener('mousemove', function(e) {

    let valueX = e.pageX - 400;
    let valueY = e.pageY - 2600;
    shape.style.top = valueY + 'px';
    shape.style.left = valueX + 'px';

});

    developerLink.addEventListener('mouseover', function() {
        developerH2.innerHTML = 'You Found it, Just Click On That!';
    });
    developerLink.addEventListener('mouseout', function() {
        developerH2.innerHTML = 'Hidden Rights';
    });

// Bubble Click
kiri.addEventListener('click', function() {
    shape.setAttribute('class', 'shape block');
    developerLink.style.visibility = 'visible';
});
kanan.addEventListener('click', function() {
    shape.setAttribute('class', 'shape none');
    developerLink.style.visibility = 'hidden';
});
kiri.addEventListener('mousedown', function() {
    kiri.style.backgroundColor = 'transparent';
});
kiri.addEventListener('mouseup', function() {
    kiri.style.backgroundColor = 'red';
});
kiri.addEventListener('mouseover', function() {
    kiri.style.cursor = 'pointer';
    kiri.style.backgroundColor = 'red';
});
kiri.addEventListener('mouseleave', function() {
    kiri.style.cursor = 'default';
    kiri.style.backgroundColor = 'transparent';
});

kanan.addEventListener('mousedown', function() {
    kanan.style.backgroundColor = 'transparent';
});
kanan.addEventListener('mouseup', function() {
    kanan.style.backgroundColor = 'blue';
});
kanan.addEventListener('mouseover', function() {
    kanan.style.cursor = 'pointer';
    kanan.style.backgroundColor = 'blue';
});
kanan.addEventListener('mouseleave', function() {
    kanan.style.cursor = 'default';
    kanan.style.backgroundColor = 'transparent';
});